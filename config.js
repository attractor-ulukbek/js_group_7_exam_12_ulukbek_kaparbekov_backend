const path = require("path");

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, "public/uploads"),
    db: {
        name: "photoGallery",
        url: "mongodb://localhost"
    },
    fb: {
        appId: "728058511465966",
        appSecret: process.env.FB_SECRET
    }
};