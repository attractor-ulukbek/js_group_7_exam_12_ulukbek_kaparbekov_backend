const mongoose = require("mongoose");
const {nanoid} = require("nanoid");
const config = require("./config");
const User = require("./models/User");
const Photo = require("./models/Photo");

mongoose.connect(config.db.url + "/" + config.db.name, { useNewUrlParser: true, useUnifiedTopology: true });

const db = mongoose.connection;

db.once("open", async () => {
    try {
        await db.dropCollection("users");
        await db.dropCollection("photos");
    } catch(e) {
        console.log("Collection were not presented, skipping drop...");
    }

    const [user1, user2] = await User.create({
        username: "user1",
        password: "1@345qWawdert",
        displayName: "John Doe",
        token: nanoid(),
    }, {
        username: "user2",
        password: "24JdwesN@njeI",
        displayName: "Джеки Чан",
        token: nanoid(),
    });

    await Photo.create({
        title: "My birhtday",
        image: "my_birthday.jpeg",
        creator: user1._id
    }, {
        title: "New Year",
        image: "new_year.jpeg",
        creator: user2._id
    });

    db.close();
});