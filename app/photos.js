const router = require("express").Router();
const multer = require("multer");
const path = require("path");
const {nanoid} = require("nanoid");
const config = require("../config");
const Photo = require("../models/Photo");
const auth = require("../middleware/auth");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get("/", async (req, res) => {
    let query;
    if (req.query.creator) {
        query = {creator: req.query.creator};
    }
    try {
        const photos = await Photo.find(query).populate("creator", "displayName");
        res.send(photos);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.post("/", [auth, upload.single("image")], async (req, res) => {
    const photoData = req.body;
    photoData.creator = req.user._id
    if (req.file) {
        photoData.image = req.file.filename;
    }
    const photo = new Photo(photoData);
    try {
        await photo.save();
        res.send(photo);
    } catch (e) {
        res.status(400).send(e);
    }
});

router.delete("/:id", [auth], async (req, res) => {
    let photo;
    let creator = req.user;
    if (req.params.id) {
        try {
            photo = await Photo.findOne({_id: req.params.id}).populate("creator", "_id");
            if (!photo) {
                res.status(301).send(req.body);
            }
            if (String(creator._id) === String(photo.creator._id)) {
                try {
                    photo = await Photo.deleteOne({_id: req.params.id})
                } catch (e) {
                    res.status(500).send(e);
                }
            }
        } catch (e) {
            res.status(500).send(e);
        }

    }
    res.send(photo)
})

module.exports = router;